#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
#include <memory>
#include "include/voxelcarving/opencv_utils.hpp"

using namespace cv;
using namespace std;

// Credits: Original version from https://github.com/opencv/opencv_contrib/tree/master/modules/aruco
namespace {
    const char* about = "Create an image with a grid of ArUco markers";
    // Parameters suggested in OpenCV ArUco source code.
    const char* keys =
        "{help usage ? |           | Print this message }"
        "{@outfile     | board.png | Output image }"
        "{w            | 3         | Number of markers in X direction }"
        "{h            | 6         | Number of markers in Y direction }"
        "{l            | 64        | Marker side length (in pixels) }"
        "{s            | 32        | Separation between two consecutive markers in the grid (in pixels)}"
        "{d            | 2         | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2,"
        "DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, "
        "DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12,"
        "DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16}"
        "{cd           |           | Input file with custom dictionary }"
        "{m            |           | Margins size (in pixels). Default is marker separation (-s) }"
        "{bb           | 1         | Number of bits in marker borders }"
        "{si           | false     | show generated image }";
}


int main(int argc, char* argv[]) {
    CommandLineParser parser(argc, argv, keys);
    parser.about(about);

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    int marker_count_x = parser.get<int>("w");
    int marker_count_y = parser.get<int>("h");
    int marker_length = parser.get<int>("l");
    int marker_separation = parser.get<int>("s");
    int margins = marker_separation;
    if (parser.has("m")) {
        margins = parser.get<int>("m");
    }

    int border_bits = parser.get<int>("bb");
    bool show_image = parser.get<bool>("si");

    String out = parser.get<String>(0);

    if (!parser.check()) {
        parser.printErrors();
        parser.printMessage();
        return 0;
    }

    Size image_size;
    image_size.width = marker_count_x * (marker_length + marker_separation) - marker_separation + 2 * margins;
    image_size.height =
        marker_count_y * (marker_length + marker_separation) - marker_separation + 2 * margins;

    shared_ptr<aruco::Dictionary> dictionary = make_shared<aruco::Dictionary>();
    if (parser.has("d")) {
        int dictionary_id = parser.get<int>("d");
        dictionary = aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME(dictionary_id));
    }
    else if (parser.has("cd")) {
        FileStorage fs(parser.get<string>("cd"), FileStorage::READ);
        bool read_ok = ::utils::read_dictionary(fs.root(), dictionary);
        if (!read_ok)
        {
            cerr << "Invalid dictionary file" << endl;
            return 0;
        }
    }
    else {
        cerr << "Dictionary not specified" << endl;
        return 0;
    }

    shared_ptr<aruco::GridBoard> board = aruco::GridBoard::create(marker_count_x, marker_count_y, float(marker_length),
        float(marker_separation), dictionary);

    // show created board
    Mat board_image;
    board->draw(image_size, board_image, margins, border_bits);

    if (show_image) {
        imshow("board", board_image);
        waitKey(0);
    }

    imwrite(out, board_image);

    return 0;
}
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <iostream>
#include <ctime>
#include <memory>
#include "include/voxelcarving/opencv_utils.hpp"

using namespace std;
using namespace cv;

// Credits: Original version from https://github.com/opencv/opencv_contrib/tree/master/modules/aruco
namespace {
    inline static bool read_camera_parameters(string filename, Mat& camera_matrix, Mat& distance_coeffs) {
        FileStorage fs(filename, FileStorage::READ);
        if (!fs.isOpened())
            return false;
        fs["camera_matrix"] >> camera_matrix;
        fs["distortion_coefficients"] >> distance_coeffs;
        return true;
    }

    inline static bool save_camera_parameters(const string& filename, Size image_size, float aspect_ratio, int flags,
        const Mat& camera_matrix, const Mat& distance_coeffs, double total_average_error) {
        FileStorage fs(filename, FileStorage::WRITE);
        if (!fs.isOpened())
            return false;

        time_t tt;
        time(&tt);
        struct tm* t2 = localtime(&tt);
        char buf[1024];
        strftime(buf, sizeof(buf) - 1, "%c", t2);

        fs << "calibration_time" << buf;
        fs << "image_width" << image_size.width;
        fs << "image_height" << image_size.height;

        if (flags & CALIB_FIX_ASPECT_RATIO) fs << "aspectRatio" << aspect_ratio;

        if (flags != 0) {
            sprintf(buf, "flags: %s%s%s%s",
                flags & CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
                flags & CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
                flags & CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
                flags & CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "");
        }
        fs << "flags" << flags;
        fs << "camera_matrix" << camera_matrix;
        fs << "distortion_coefficients" << distance_coeffs;
        fs << "avg_reprojection_error" << total_average_error;
        return true;
    }

    const char* about =
        "Get the camera intrinsics using an ArUco Grid printout\n"
        "  Press 'c' to get a single frame from camera feed,\n"
        "  or press any key for next frame from video feed\n"
        "  then press 'esc' to finish capturing and start calibration.\n";
    // Parameters suggested in OpenCV ArUco source code.
    const char* keys =
        "{help usage ? |          | Print this message }"
        "{w            | 3        | Number of squares in X direction }"
        "{h            | 6        | Number of squares in Y direction }"
        "{l            | 0.0135   | Marker side length (in meters) }"
        "{s            | 0.0065   | Separation between two consecutive markers in the grid (in meters) }"
        "{d            | 2        | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2,"
        "DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, "
        "DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12,"
        "DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16}"
        "{cd           |          | Input file with custom dictionary }"
        "{@outfile     | cam.txt  | Output file with calibrated camera parameters }"
        "{v            |          | Input from video file, if ommited, input comes from camera }"
        "{ci           | 0        | Camera id if input doesnt come from video (-v) }"
        "{dp           |          | File of marker detector parameters }"
        "{rs           | false    | Apply refind strategy }"
        "{zt           | false    | Assume zero tangential distortion }"
        "{a            |          | Fix aspect ratio (fx/fy) to this value }"
        "{pc           | false    | Fix the principal point at the center }";
}


int main(int argc, char* argv[]) {
    CommandLineParser parser(argc, argv, keys);
    parser.about(about);

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    int marker_count_x = parser.get<int>("w");
    int marker_count_y = parser.get<int>("h");
    float marker_length = parser.get<float>("l");
    float marker_separation = parser.get<float>("s");
    string output_file = parser.get<String>(0);

    int calibration_flags = 0;
    float aspect_ratio = 1;
    if (parser.has("a")) {
        calibration_flags |= CALIB_FIX_ASPECT_RATIO;
        aspect_ratio = parser.get<float>("a");
    }
    if (parser.get<bool>("zt")) calibration_flags |= CALIB_ZERO_TANGENT_DIST;
    if (parser.get<bool>("pc")) calibration_flags |= CALIB_FIX_PRINCIPAL_POINT;

    shared_ptr<aruco::DetectorParameters> detector_params = make_shared<aruco::DetectorParameters>();
    if (parser.has("dp")) {
        FileStorage fs(parser.get<string>("dp"), FileStorage::READ);
        bool read_ok = ::utils::read_detector_parameters(fs.root(), detector_params);
        if (!read_ok) {
            cerr << "Invalid detector parameters file" << endl;
            return 0;
        }
    }

    bool refind_strategy = parser.get<bool>("rs");
    int cam_id = parser.get<int>("ci");
    String video;

    if (parser.has("v")) {
        video = parser.get<String>("v");
    }

    if (!parser.check()) {
        parser.printErrors();
        parser.printMessage();
        return 0;
    }

    VideoCapture input_video;
    int wait_time;
    if (!video.empty()) {
        input_video.open(video);
        wait_time = 0;
    }
    else {
        input_video.open(cam_id);
        wait_time = 10;
    }

    shared_ptr<aruco::Dictionary> dictionary;
    if (parser.has("d")) {
        int dictionary_id = parser.get<int>("d");
        dictionary = aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME(dictionary_id));
    }
    else if (parser.has("cd")) {
        FileStorage fs(parser.get<string>("cd"), FileStorage::READ);
        bool read_ok = ::utils::read_dictionary(fs.root(), dictionary);
        if (!read_ok) {
            cerr << "Invalid dictionary file" << endl;
            return 0;
        }
    }
    else {
        cerr << "Dictionary not specified" << endl;
        return 0;
    }

    // create board object
    shared_ptr<aruco::GridBoard> gridboard =
        aruco::GridBoard::create(marker_count_x, marker_count_y, marker_length, marker_separation, dictionary);
    shared_ptr<aruco::Board> board = static_cast<shared_ptr<aruco::Board>>(gridboard);

    // collected frames for calibration
    vector< vector< vector< Point2f > > > all_corners;
    vector< vector< int > > all_ids;
    Size img_size;

    while (input_video.grab()) {
        Mat image, image_copy;
        input_video.retrieve(image);

        vector< int > ids;
        vector< vector< Point2f > > corners, rejected;
        // detect markers
        aruco::detectMarkers(image, dictionary, corners, ids, detector_params, rejected);
        // refind strategy to detect more markers
        if (refind_strategy) aruco::refineDetectedMarkers(image, board, corners, ids, rejected);

        // draw results
        image.copyTo(image_copy);
        if (ids.size() > 0) aruco::drawDetectedMarkers(image_copy, corners, ids);
        putText(image_copy, "Press 'c' to add current frame. 'ESC' to finish and calibrate",
            Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 0, 0), 2);

        imshow("out", image_copy);
        char key = (char)waitKey(wait_time);
        if (key == 27) break;
        if (key == 'c' && ids.size() > 0) {
            cout << "Frame captured" << endl;
            all_corners.push_back(corners);
            all_ids.push_back(ids);
            img_size = image.size();
        }
    }

    if (all_ids.size() < 1) {
        cerr << "Not enough captures for calibration" << endl;
        return 0;
    }

    Mat camera_matrix, distance_coeffs;
    vector< Mat > rvecs, tvecs;
    double rep_error;

    if (calibration_flags & CALIB_FIX_ASPECT_RATIO) {
        camera_matrix = Mat::eye(3, 3, CV_64F);
        camera_matrix.at< double >(0, 0) = aspect_ratio;
    }

    // prepare data for calibration
    vector< vector< Point2f > > all_corners_concatenated;
    vector< int > all_ids_concatenated;
    vector< int > marker_counter_per_frame;
    marker_counter_per_frame.reserve(all_corners.size());
    for (unsigned int i = 0; i < all_corners.size(); i++) {
        marker_counter_per_frame.push_back((int)all_corners[i].size());
        for (unsigned int j = 0; j < all_corners[i].size(); j++) {
            all_corners_concatenated.push_back(all_corners[i][j]);
            all_ids_concatenated.push_back(all_ids[i][j]);
        }
    }
    // calibrate camera
    rep_error = aruco::calibrateCameraAruco(all_corners_concatenated, all_ids_concatenated,
        marker_counter_per_frame, board, img_size, camera_matrix,
        distance_coeffs, rvecs, tvecs, calibration_flags);

    bool save_ok = save_camera_parameters(output_file, img_size, aspect_ratio, calibration_flags, camera_matrix,
        distance_coeffs, rep_error);

    if (!save_ok) {
        cerr << "Cannot save output file" << endl;
        return 0;
    }

    cout << "Rep Error: " << rep_error << endl;
    cout << "Calibration saved to " << output_file << endl;

    return 0;
}
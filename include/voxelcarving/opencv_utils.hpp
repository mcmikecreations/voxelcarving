#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
#include <memory>

namespace utils
{
    inline void kill(int code, const char* message)
    {
        std::cerr << message << std::endl;
        exit(code);
    }

    template<typename T>
    inline bool read_parameter(const cv::FileNode& node, T& parameter)
    {
        if (!node.empty())
        {
            node >> parameter;
            return true;
        }
        return false;
    }

    inline bool read_detector_parameters(const cv::FileNode& fn, std::shared_ptr<cv::aruco::DetectorParameters>& params)
    {
        if (fn.empty())
            return true;
        params = std::make_shared<cv::aruco::DetectorParameters>();
        bool check_read = false;
        check_read |= read_parameter(fn["adaptiveThreshWinSizeMin"], params->adaptiveThreshWinSizeMin);
        check_read |= read_parameter(fn["adaptiveThreshWinSizeMax"], params->adaptiveThreshWinSizeMax);
        check_read |= read_parameter(fn["adaptiveThreshWinSizeStep"], params->adaptiveThreshWinSizeStep);
        check_read |= read_parameter(fn["adaptiveThreshConstant"], params->adaptiveThreshConstant);
        check_read |= read_parameter(fn["minMarkerPerimeterRate"], params->minMarkerPerimeterRate);
        check_read |= read_parameter(fn["maxMarkerPerimeterRate"], params->maxMarkerPerimeterRate);
        check_read |= read_parameter(fn["polygonalApproxAccuracyRate"], params->polygonalApproxAccuracyRate);
        check_read |= read_parameter(fn["minCornerDistanceRate"], params->minCornerDistanceRate);
        check_read |= read_parameter(fn["minDistanceToBorder"], params->minDistanceToBorder);
        check_read |= read_parameter(fn["minMarkerDistanceRate"], params->minMarkerDistanceRate);
        check_read |= read_parameter(fn["cornerRefinementMethod"], params->cornerRefinementMethod);
        check_read |= read_parameter(fn["cornerRefinementWinSize"], params->cornerRefinementWinSize);
        check_read |= read_parameter(fn["cornerRefinementMaxIterations"], params->cornerRefinementMaxIterations);
        check_read |= read_parameter(fn["cornerRefinementMinAccuracy"], params->cornerRefinementMinAccuracy);
        check_read |= read_parameter(fn["markerBorderBits"], params->markerBorderBits);
        check_read |= read_parameter(fn["perspectiveRemovePixelPerCell"], params->perspectiveRemovePixelPerCell);
        check_read |= read_parameter(fn["perspectiveRemoveIgnoredMarginPerCell"], params->perspectiveRemoveIgnoredMarginPerCell);
        check_read |= read_parameter(fn["maxErroneousBitsInBorderRate"], params->maxErroneousBitsInBorderRate);
        check_read |= read_parameter(fn["minOtsuStdDev"], params->minOtsuStdDev);
        check_read |= read_parameter(fn["errorCorrectionRate"], params->errorCorrectionRate);
        return check_read;
    }

    inline bool read_dictionary(const cv::FileNode& fn, std::shared_ptr<cv::aruco::Dictionary>& dictionary)
    {
        int marker_count = 0, marker_size = 0;
        if (fn.empty() || !read_parameter(fn["nmarkers"], marker_count) || !read_parameter(fn["markersize"], marker_size))
            return false;
        cv::Mat bytes(0, 0, CV_8UC1), marker(marker_size, marker_size, CV_8UC1);
        std::string marker_string;
        for (int i = 0; i < marker_count; i++)
        {
            std::ostringstream ostr;
            ostr << i;
            if (!read_parameter(fn["marker_" + ostr.str()], marker_string))
                return false;

            for (int j = 0; j < (int)marker_string.size(); j++)
                marker.at<unsigned char>(j) = (marker_string[j] == '0') ? 0 : 1;
            bytes.push_back(cv::aruco::Dictionary::getByteListFromBits(marker));
        }
        dictionary = std::make_shared<cv::aruco::Dictionary>(bytes, marker_size);
        return true;
    }

    class frame_data
    {
    public:
        std::vector<int> ids;
        std::vector<std::vector<cv::Point2f>> corners;
    };

    void parse_2d_tracks(const std::vector<frame_data>& total_frame_data, std::vector<cv::Mat>& points2d)
    {
        int n_frames = 0, n_tracks = 0;
        n_frames = total_frame_data.size();
        auto first_corners_it = std::find_if(total_frame_data.begin(), total_frame_data.end(), [](const frame_data& data)
            {
                return !data.corners.empty();
            });
        if (first_corners_it == total_frame_data.end()) kill(-1, "No markers found.");
        int n_corners = first_corners_it->corners[0].size();

        // Get frame with biggest marker id to find marker count.
        auto& biggest_frame_it = std::max_element(
            total_frame_data.begin(),
            total_frame_data.end(),
            [](const frame_data& l, const frame_data& r)
            {
                auto max_l = std::max_element(l.ids.begin(), l.ids.end(), [](int ll, int lr)
                    {
                        return ll < lr;
                    });
                auto max_r = std::max_element(r.ids.begin(), r.ids.end(), [](int rl, int rr)
                    {
                        return rl < rr;
                    });
                return max_l < max_r;
            });
        auto& biggest_frame = *biggest_frame_it;
        // corners * (max id + 1)
        n_tracks = n_corners * (1 + *std::max_element(
            biggest_frame.ids.begin(),
            biggest_frame.ids.end()
        ));

        for (std::size_t i = 0; i < total_frame_data.size(); ++i)
        {
            auto& frame_data = total_frame_data[i];
            cv::Mat_<float> frame(2, n_tracks, -1.0f);

            for (std::size_t id_index = 0; id_index < frame_data.ids.size(); ++id_index)
            {
                int id = frame_data.ids[id_index];
                if (frame_data.corners.size() <= id_index) kill(-1, "Not enough markers in input data.");
                auto& corners = frame_data.corners[id_index];

                for (std::size_t corner_index = 0; corner_index < corners.size(); ++corner_index)
                {
                    if (corners.size() <= corner_index) kill(-1, "Not enough corners for a marker in input data.");
                    int j = id * n_corners + corner_index;

                    frame(0, j) = corners[corner_index].x;
                    frame(1, j) = corners[corner_index].y;
                }
            }

            points2d.push_back(cv::Mat(frame));
        }
    }

    template <typename T>
    T mod(T num, T base)
    {
        T ret = fmod(num, base);
        return (ret >= 0) ? (ret) : (ret + base);
    }
}

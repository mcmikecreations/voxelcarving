#pragma once

#include <memory>
class VoxelVolumeImpl;

class VoxelVolume
{
public:
	/// <summary>
	/// Set a voxel to some value. Coordinates are relative to volume size.
	/// </summary>
	void SetVoxel(float tx, float ty, float tz, float r, float g, float b);
	void SetVoxel(int x, int y, int z, float r, float g, float b);
	void ClearVoxel(float tx, float ty, float tz);
	//void Fill(float r, float g, float b);
	void Init();
	void Clear();

	float* GetRawVoxelArray();

	float GetSizeX();
	float GetSizeY();
	float GetSizeZ();
	float GetCellSize();

	int GetCellCountX();
	int GetCellCountY();
	int GetCellCountZ();
	int GetCellCountTotal();

	VoxelVolume(float sx, float sy, float sz, float cellSize);
	~VoxelVolume();
	VoxelVolume() = delete;
	VoxelVolume(const VoxelVolume&) = delete;
	VoxelVolume(VoxelVolume&&) = delete;
private:
	std::unique_ptr<VoxelVolumeImpl> m_impl;
};

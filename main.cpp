#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkCleanPolyData.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyData.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkInformation.h>
#include <vtkMarchingCubes.h>
#include <vtkMergePoints.h>
#include <vtkMetaImageReader.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkOutlineFilter.h>
#include <vtkPLYWriter.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataWriter.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkStructuredPoints.h>
#include <vtkVersion.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>

#include "Eigen.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <sstream>
#include <string>
#include <regex>
#include <memory>
#include <iterator>
#include <stdlib.h>

#include "include/voxelcarving/opencv_utils.hpp"

#pragma region Utilities

namespace {
	const int OUT_MODE_OPENCV = 0;
	const int OUT_MODE_POSEVIZ = 1;
	const int OUT_MODE_VTK = 2;
	const int OUT_MODE_FILE = 3;
	const int WAIT_TIME = 10;

	inline bool get_image(cv::Mat& output, std::string path)
	{
		output = cv::imread(path);
		return output.data != NULL;
	}

	inline bool read_camera_parameters(std::string filename, cv::Mat& camMatrix, cv::Mat& distCoeffs)
	{
		cv::FileStorage fs(filename, cv::FileStorage::READ);
		if (!fs.isOpened())
			return false;
		fs["camera_matrix"] >> camMatrix;
		fs["distortion_coefficients"] >> distCoeffs;
		return true;
	}

	struct image_video
	{
	public:
		cv::VideoCapture* video;
		int id;
		std::string path;
	};

	struct image_memory
	{
	public:
		std::vector<cv::Mat>* images;
		std::size_t count;
		std::size_t current;
	};

	struct image_images
	{
	public:
		std::string base_path;
		std::size_t count;
		std::size_t current;
		std::size_t step;
	};

	bool get_image_video(cv::Mat& image, void* data)
	{
		image_video* input = static_cast<image_video*>(data);
		if (input->video->grab() == false) {
			return false;
		}
		else {
			input->video->retrieve(image);
			return image.data != NULL;
		}
	}

	bool get_image_memory(cv::Mat& image, void* data)
	{
		image_memory* input = static_cast<image_memory*>(data);
		if (input->current >= input->count)
		{
			return false;
		}
		else {
			input->images->operator[](input->current).copyTo(image);
			return image.data != NULL;
		}
	}

	bool get_image_images(cv::Mat& image, void* data)
	{
		image_images* input = static_cast<image_images*>(data);

		if (input->current / input->step >= input->count) return false;
		int image_index = (input->current / input->step) % input->count;

		bool result = get_image(image, input->base_path + std::to_string(image_index) + ".jpg");
		input->current = (input->current + 1) % (input->count * input->step * 2);
		return result;
	}

	void setup_image_data(
		const cv::CommandLineParser& parser,
		const std::string& inmode,
		bool (**image_grab)(cv::Mat&, void*),
		void** image_data,
		image_video* image_data_video,
		image_images* image_data_images,
		image_memory* image_data_memory,
		cv::VideoCapture* image_data_video_input,
		std::vector<cv::Mat>* image_data_memory_input
	)
	{
		if (inmode == "images")
		{
			*image_grab = get_image_images;
			*image_data = image_data_images;

			image_data_images->base_path = parser.get<std::string>("inimgpath");
			image_data_images->count = parser.get<int>("inimgcount");
			image_data_images->current = 0;
		}
		else if (inmode == "video")
		{
			*image_grab = get_image_video;
			*image_data = image_data_video;

			image_data_video->path = parser.get<std::string>("invideo");
			image_data_video_input->open(image_data_video->path);
			image_data_video->video = image_data_video_input;
		}
		else if (inmode == "camera")
		{
			*image_grab = get_image_video;
			*image_data = image_data_video;

			image_data_video->id = parser.get<int>("incamid");
			image_data_video_input->open(image_data_video->id);
			image_data_video->video = image_data_video_input;
		}
		else if (inmode == "memory")
		{
			*image_grab = get_image_memory;
			*image_data = image_data_memory;

			image_data_memory->images = image_data_memory_input;
			image_data_memory->count = image_data_memory_input->size();
			image_data_memory->current = 0;
		}
		else
		{
			::utils::kill(-1, "Wrong {inmode}.");
		}
	}

	const char* about = "Perform voxel carving";
	// Parameters suggested in OpenCV ArUco source code.
	const char* keys =
		"{help usage ? |           | Print this message }"
		"{inmode       | images    | Input mode: images, video, camera. Only jpg images are supported }"
		"{outmode      | 0         | Output mode: OPENCV=0, POSEVIZ=1, VTK=2, FILE=3 }"
		"{d            | 2         | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2,"
		"DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, "
		"DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12,"
		"DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16,"
		"DICT_APRILTAG_16h5=17, DICT_APRILTAG_25h9=18, DICT_APRILTAG_36h10=19, DICT_APRILTAG_36h11=20 }"
		"{cd           |           | Input file with custom marker dictionary }"
		"{invideo      | <none>    | Input of video path }"
		"{outfile      | res.ply   | Output file path }"
		"{inimgpath    |           | Input of image folder path }"
		"{inimgcount   |           | Input of image count }"
		"{incamid      |           | Input of camera id }"
		"{c            | cam.txt   | Camera intrinsic parameters. Needed for camera pose }"
		"{l            | 0.0135    | Marker side length (in meters). Needed for correct scale in camera pose }"
		"{dp           |           | File of marker detector parameters }"
		"{bgcolor      | faa48f    | Background color to carve in HSV hex format HHSSVV }"
		"{bgdelta      | 4d878e    | Delta color to carve in HSV hex format HHSSVV }"
		"{voxelsx      | 256       | Voxel structure length }"
		"{voxelsy      | 256       | Voxel structure width }"
		"{voxelsz      | 256       | Voxel structure height }"
		"{voxelsc      | 0.0135    | Voxel structure cell size }"
		"{refine       | 2         | Corner refinement: CORNER_REFINE_NONE=0, CORNER_REFINE_SUBPIX=1,"
		"CORNER_REFINE_CONTOUR=2, CORNER_REFINE_APRILTAG=3 }";

	static bool camera_pov = false;

	static void viz_keyboard_callback(const cv::viz::KeyboardEvent& event, void* cookie)
	{
		if (event.action == 0 && !event.symbol.compare("s"))
			camera_pov = !camera_pov;
	}

	struct coord {
		int x;
		int y;
	};

	struct voxel {
		float xpos;
		float ypos;
		float zpos;
	};

	coord project(voxel v, Eigen::Matrix<double, 4, 4> P) {
		coord im;
		Eigen::Vector4d voxel = { v.xpos,v.ypos,v.zpos,1 };
		Eigen::Vector4d projectedP = P * voxel;
		//projectedP = K * projectedP;
		projectedP = projectedP / projectedP[2];

		im.y = std::roundf((float)projectedP[1]);
		im.x = std::roundf((float)projectedP[0]);
		return im;
	}

	coord project(voxel v, Eigen::Matrix<double, 4, 4> E, Eigen::Matrix<double, 3, 3> K) {
		coord im;
		Eigen::Vector4d voxel = { v.xpos,v.ypos,v.zpos,1 };
		Eigen::Vector4d projectedP = E * voxel;
		Eigen::Vector3d pp = { projectedP[0],projectedP[1],projectedP[2] };
		pp = K * pp;
		//projectedP = K * projectedP;
		pp = pp / pp[2];

		im.y = std::roundf((float)pp[1]);
		im.x = std::roundf((float)pp[0]);
		return im;
	}


	struct startParams {
		float startX;
		float startY;
		float startZ;
		float voxelWidth;
		float voxelHeight;
		float voxelDepth;
	};

	cv::Mat createDistImage(cv::Mat silhouette) {
		cv::Mat sil, distImage;
		cv::Canny(silhouette, sil, 0, 255);
		cv::bitwise_not(sil, sil);
		cv::distanceTransform(sil, distImage, 2, 3);
		return distImage;
	}

	Eigen::Matrix<double, 4, 4> getProjectionMatrix(const Eigen::Matrix<double, 4, 4>& extrinsic, const Eigen::Matrix<double, 4, 4>& intrinsic) {
		return intrinsic * extrinsic;
	}

	float* carve(
		const cv::CommandLineParser& parser,
		float* distances,
		float size_x, float size_y, float size_z,
		int dim_x, int dim_y, int dim_z,
		bool (*image_grab)(cv::Mat&, void*),
		void* image_data,
		const cv::Scalar& bg_color_min, const cv::Scalar& bg_color_max,
		const std::vector<cv::Mat>& Rs_est,
		const std::vector<cv::Mat>& ts_est,
		const cv::Matx33f& camera_matrix
	) {
		float xmin = -size_x / 2.0f, ymin = -size_y / 2.0f, zmin = -size_z / 2.0f;
		float xmax = size_x / 2.0f, ymax = size_y / 2.0f, zmax = size_z / 2.0f;

		float bbwidth = std::abs(xmax - xmin);
		float bbheight = std::abs(ymax - ymin);
		float bbdepth = std::abs(zmax - zmin);


		startParams params;
		params.startX = xmin;
		params.startY = ymin;
		params.startZ = zmin;
		params.voxelWidth = bbwidth / dim_x;
		params.voxelHeight = bbheight / dim_y;
		params.voxelDepth = bbdepth / dim_z;

		cv::Mat image, image_copy;
		cv::Mat silhouette, silhouette_copy;

		std::vector<utils::frame_data> total_frame_data;
		int frame_index = 0;

		auto voxel_slice = dim_z * dim_y;

		auto camera_intrinsics = cv::Matx34d::zeros();
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				camera_intrinsics(i,j) = (double)camera_matrix(i, j);

		cv::Matx44d camera_extrinsics_current = cv::Matx44d::eye();
		std::cout << camera_intrinsics << std::endl;

		while (image_grab(image, image_data))
		{
			cv::cvtColor(image, silhouette, cv::COLOR_BGR2HSV);
			cv::inRange(silhouette, bg_color_min, bg_color_max, silhouette);

			//cv::imshow("Test", silhouette_copy);
			cv::Matx44d camera_extrinsics = cv::Affine3d(Rs_est[frame_index], ts_est[frame_index]).matrix;
			//std::cout << camera_extrinsics << std::endl;
			camera_extrinsics_current = camera_extrinsics * camera_extrinsics_current;
			//auto P = getProjectionMatrix(extrinsics, intrinsics);

			for (int i = 0; i < dim_x; i++) {
				for (int j = 0; j < dim_y; j++) {
					for (int k = 0; k < dim_z; k++) {
						voxel v;
						v.xpos = params.startX + i * params.voxelWidth;
						v.ypos = params.startY + j * params.voxelHeight;
						v.zpos = params.startZ + k * params.voxelDepth;
						cv::Vec4d vWorld(v.xpos, v.ypos, v.zpos, 1.0f);

						float dist = distances[i * voxel_slice + j * dim_z + k];
						//std::cout << "Inside" << std::endl;
						//float dist = -1.0f;
						auto vScreen = camera_intrinsics * camera_extrinsics * vWorld;
						vScreen /= std::abs(vScreen(2));
						//vScreen(0) += camera_intrinsics(0, 2);
						//vScreen(1) += camera_intrinsics(1, 2);
						//std::cout << vScreen << std::endl;
						if (vScreen(0) >= 0.0 && vScreen(1) >= 0.0 && vScreen(0) < silhouette.cols && vScreen(1) < silhouette.rows && vScreen(2) >= 0.0)
						{
							auto pixel = silhouette.at<uchar>((int)vScreen(1), (int)vScreen(0));
							//if ((int)vScreen(1) % 2 == 1)
								//dist = -std::abs(dist);
							if (pixel <= 150) dist = -std::abs(dist);
						}
						else
						{
							dist = -std::abs(dist);
						}
						distances[i * voxel_slice + j * dim_z + k] = dist;
					}
				}
			}
			std::cout << "Frame: " << frame_index << std::endl;
			//if (frame_index == 4) break;
			++frame_index;
		}
		return distances;
	}
}

#pragma endregion

int main(int argc, char* argv[])
{
	cv::CommandLineParser parser(argc, argv, keys);
	parser.about(about);

	if (parser.has("help"))
	{
		parser.printMessage();
		return 0;
	}

#pragma region Setup

	bool estimate_pose = parser.has("c");
	cout << "Camera intrinsics: " << (estimate_pose ? parser.get<std::string>("c") : "no") << endl;
	float marker_length = parser.get<float>("l");

	auto detector_params = std::make_shared< cv::aruco::DetectorParameters >();
	if (parser.has("dp"))
	{
		cv::FileStorage fs(parser.get<std::string>("dp"), cv::FileStorage::READ);
		bool read_ok = ::utils::read_detector_parameters(fs.root(), detector_params);
		if (!read_ok)
		{
			cerr << "Invalid detector parameters file" << endl;
			return 0;
		}
	}
	if (parser.has("refine"))
	{
		// Override cornerRefinementMethod read from config file
		detector_params->cornerRefinementMethod = parser.get<int>("refine");
	}

	int outmode = parser.get<int>("outmode");

	image_video image_data_video;
	image_images image_data_images;
	image_memory image_data_memory;
	cv::VideoCapture image_data_video_input;
	std::vector<cv::Mat> image_data_memory_input;
	void* image_data = nullptr;
	bool (*image_grab)(cv::Mat&, void*) = nullptr;

	image_data_images.step = outmode == 0 ? 30 : 1;
	std::string inmode = parser.get<std::string>("inmode");
	setup_image_data(
		parser,
		inmode,
		&image_grab,
		&image_data,
		&image_data_video,
		&image_data_images,
		&image_data_memory,
		&image_data_video_input,
		&image_data_memory_input
	);

	cv::Scalar bg_color;
	cv::Scalar bg_delta;
	{
		int h, s, v;
		sscanf(parser.get<std::string>("bgcolor").c_str(), "%02x%02x%02x", &h, &s, &v);
		bg_color = cv::Scalar(h, s, v);
		sscanf(parser.get<std::string>("bgdelta").c_str(), "%02x%02x%02x", &h, &s, &v);
		bg_delta = cv::Scalar(h, s, v);
	}
	cv::Scalar bg_color_min(
		//::utils::mod<double>(bg_color[0] - bg_delta[0], 256.0),
		std::clamp<double>(bg_color[0] - bg_delta[0], 0, 255),
		std::clamp<double>(bg_color[1] - bg_delta[1], 0, 255),
		std::clamp<double>(bg_color[2] - bg_delta[2], 0, 255));
	cv::Scalar bg_color_max(
		//::utils::mod<double>(bg_color[0] + bg_delta[0], 256.0),
		std::clamp<double>(bg_color[0] + bg_delta[0], 0, 255),
		std::clamp<double>(bg_color[1] + bg_delta[1], 0, 255),
		std::clamp<double>(bg_color[2] + bg_delta[2], 0, 255));

	if (!parser.check())
	{
		parser.printErrors();
		parser.printMessage();
		return 0;
	}

	int voxel_size_x = parser.get<int>("voxelsx");
	int voxel_size_y = parser.get<int>("voxelsy");
	int voxel_size_z = parser.get<int>("voxelsz");
	float voxel_size_cell = parser.get<float>("voxelsc");

	auto dictionary = std::make_shared<cv::aruco::Dictionary>();
	if (parser.has("d"))
	{
		int dictionary_id = parser.get<int>("d");
		dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionary_id));
	}
	else if (parser.has("cd"))
	{
		cv::FileStorage fs(parser.get<std::string>("cd"), cv::FileStorage::READ);
		bool read_ok = ::utils::read_dictionary(fs.root(), dictionary);
		if (!read_ok)
		{
			std::cerr << "Invalid dictionary file" << std::endl;
			return 0;
		}
	}
	else
	{
		std::cerr << "Dictionary not specified" << std::endl;
		return 0;
	}

	cv::Mat camera_matrix, distance_coeffs, camera_matrix_copy;
	if (estimate_pose)
	{
		bool read_ok = read_camera_parameters(parser.get<std::string>("c"), camera_matrix, distance_coeffs);
		if (!read_ok)
		{
			cerr << "Invalid camera file" << endl;
			return 0;
		}
		camera_matrix.copyTo(camera_matrix_copy);
	}

#pragma endregion

	double total_time = 0;
	int total_iterations = 0;
	cv::Mat image, image_copy;
	cv::Mat silhouette, silhouette_copy;
	cv::Size image_size = cv::Size(640, 480);
	cv::Mat result(image_size.height, image_size.width * 2, CV_8UC3, cv::Scalar(0, 0, 0));

	std::vector<utils::frame_data> total_frame_data;

	while (image_grab(image, image_data))
	{
		double tick = (double)cv::getTickCount();

		total_frame_data.push_back({});
		utils::frame_data& current_frame_data = total_frame_data.back();
		if (inmode == "camera") image_data_memory_input.push_back(image);

		std::vector<std::vector<cv::Point2f>> rejected;
		std::vector<cv::Vec3d> rvecs, tvecs;

		cv::cvtColor(image, silhouette, cv::COLOR_BGR2HSV);
		cv::inRange(silhouette, bg_color_min, bg_color_max, silhouette);

		// detect markers and estimate pose
		cv::aruco::detectMarkers(image, dictionary, current_frame_data.corners, current_frame_data.ids, detector_params, rejected);
		if (estimate_pose && current_frame_data.ids.size() > 0)
			cv::aruco::estimatePoseSingleMarkers(current_frame_data.corners, marker_length, camera_matrix, distance_coeffs, rvecs,
				tvecs);

		// get camera extrinsics
		/*cv::Mat camera_extrinsics(4, 4, CV_32FC1, cv::Scalar(0, 0, 0));
		{
			auto it = std::min_element(std::begin(current_frame_data.ids), std::end(current_frame_data.ids));
			int index = std::distance(std::begin(current_frame_data.ids), it);
			cv::Mat marker_rotation;
			cv::Rodrigues(rvecs[index], marker_rotation);
			marker_rotation.copyTo(camera_extrinsics(cv::Rect(0, 0, 3, 3)));
			camera_extrinsics.at<float>(3, 0) = tvecs[index][0];
			camera_extrinsics.at<float>(3, 1) = tvecs[index][1];
			camera_extrinsics.at<float>(3, 2) = tvecs[index][2];
			camera_extrinsics.at<float>(3, 3) = 1.0f;
			camera_extrinsics = camera_extrinsics.inv();
			camera_extrinsics_list.push_back(camera_extrinsics);
		}*/

		// TODO: carve voxel volume

		double current_time = ((double)cv::getTickCount() - tick) / cv::getTickFrequency();
		total_time += current_time;
		total_iterations++;
		if (total_iterations % image_data_images.step == 0)
		{
			cout << "Detection Time = " << current_time * 1000 << " ms "
				<< "(Mean = " << 1000 * total_time / double(total_iterations) << " ms)" << endl;
		}

		if (current_frame_data.ids.size() > 0)
		{
			cv::aruco::drawDetectedMarkers(image, current_frame_data.corners, current_frame_data.ids);

			if (estimate_pose)
			{
				for (unsigned int i = 0; i < current_frame_data.ids.size(); i++)
					cv::aruco::drawAxis(image, camera_matrix, distance_coeffs, rvecs[i], tvecs[i],
						marker_length * 0.5f);
			}
		}

		if (outmode == OUT_MODE_OPENCV || inmode == "camera")
		{
			cv::resize(image, image_copy, image_size);
			cv::resize(silhouette, silhouette_copy, image_size);
			cv::cvtColor(silhouette_copy, silhouette, cv::COLOR_GRAY2BGR);
			image_copy.copyTo(result(cv::Rect(0, 0, image_copy.cols, image_copy.rows)));
			silhouette.copyTo(result(cv::Rect(silhouette.cols, 0, silhouette.cols, silhouette.rows)));

			cv::imshow("Result", result);

			char key = (char)cv::waitKey(WAIT_TIME);
			if (key == 'q') break;
		}
	}
	// We showed OpenCV output, goodbye
	if (outmode == OUT_MODE_OPENCV) ::utils::kill(0, "Successfully ran OpenCV preview.");
	// Process frame data
	// What we have:
	// - ids of detected markers for each frame
	// - 2d camera space positions of 4 corners of detected markers for each frame
	// - camera intrinsics in camera_matrix
	// What we need:
	// - a cv::Mat array where each element is all points positions per frame
	// What we do:
	// - calculate this array based on https://docs.opencv.org/3.4/d5/dab/tutorial_sfm_trajectory_estimation.html
	// - call cv::reconstruct to get camera position and rotation for each frame
	// - visualize this
	std::vector<cv::Mat> points2d;
	if (total_frame_data.empty()) ::utils::kill(-1, "No markers detected.");
	::utils::parse_2d_tracks(total_frame_data, points2d);

	bool is_projective = true;
	std::vector<cv::Mat> Rs_est, ts_est, points3d_estimated;
	cv::sfm::reconstruct(points2d, Rs_est, ts_est, camera_matrix, points3d_estimated, is_projective);

	if (outmode == OUT_MODE_POSEVIZ)
	{
		std::cout << "\n----------------------------\n" << std::endl;
		std::cout << "Reconstruction: " << std::endl;
		std::cout << "============================" << std::endl;
		std::cout << "Estimated 3D points: " << points3d_estimated.size() << std::endl;
		std::cout << "Estimated cameras: " << Rs_est.size() << std::endl;
		std::cout << "Refined intrinsics: " << std::endl << camera_matrix << std::endl << std::endl;

		cout << "3D Visualization: " << endl;
		cout << "============================" << endl;


		/// Create 3D windows
		cv::viz::Viz3d window_est("Estimation Coordinate Frame");
		window_est.setBackgroundColor(); // black by default
		window_est.registerKeyboardCallback(&viz_keyboard_callback);

		// Create the pointcloud
		cout << "Recovering points  ... ";

		// recover estimated points3d
		std::vector<cv::Vec3f> point_cloud_est;
		for (int i = 0; i < points3d_estimated.size(); ++i)
			point_cloud_est.push_back(cv::Vec3f(points3d_estimated[i]));

		std::cout << "[DONE]" << endl;


		/// Recovering cameras
		std::cout << "Recovering cameras ... ";

		std::vector<cv::Affine3d> path_est;
		for (size_t i = 0; i < Rs_est.size(); ++i)
			path_est.push_back(cv::Affine3d(Rs_est[i], ts_est[i]));

		std::cout << "[DONE]" << std::endl;

		/// Add cameras
		std::cout << "Rendering Trajectory  ... ";

		/// Wait for key 'q' to close the window
		std::cout << std::endl << "Press:                       " << std::endl;
		std::cout << " 's' to switch the camera pov" << std::endl;
		std::cout << " 'q' to close the windows    " << std::endl;


		if (path_est.size() > 0)
		{
			// animated trajectory
			int idx = 0, forw = -1, n = static_cast<int>(path_est.size());

			while (!window_est.wasStopped())
			{
				/// Render points as 3D cubes
				for (size_t i = 0; i < point_cloud_est.size(); ++i)
				{
					cv::Vec3d point = point_cloud_est[i];
					cv::Affine3d point_pose(cv::Mat::eye(3, 3, CV_64F), point);

					char buffer[50];
					sprintf(buffer, "%d", static_cast<int>(i));

					cv::viz::WCube cube_widget(cv::Point3f(0.1, 0.1, 0.0), cv::Point3f(0.0, 0.0, -0.1), true, cv::viz::Color::blue());
					cube_widget.setRenderingProperty(cv::viz::LINE_WIDTH, 2.0);
					window_est.showWidget("Cube" + cv::String(buffer), cube_widget, point_pose);
				}

				{
					float xmin = -voxel_size_x * voxel_size_cell / 2.0f, ymin = -voxel_size_y * voxel_size_cell / 2.0f, zmin = -voxel_size_z * voxel_size_cell / 2.0f;
					float xmax = voxel_size_x * voxel_size_cell / 2.0f, ymax = voxel_size_y * voxel_size_cell / 2.0f, zmax = voxel_size_z * voxel_size_cell / 2.0f;

					float bbwidth = std::abs(xmax - xmin);
					float bbheight = std::abs(ymax - ymin);
					float bbdepth = std::abs(zmax - zmin);

					cv::viz::WCube cube_widget(cv::Point3f(xmin, ymin, zmin), cv::Point3f(xmax, ymax, zmax), true, cv::viz::Color::red());
					cube_widget.setRenderingProperty(cv::viz::LINE_WIDTH, 2.0);
					//window_est.showWidget("Cube" + cv::String("Volume"), cube_widget, cv::Affine3d());
					window_est.showWidget("Cube" + cv::String("Volume"), cube_widget);
				}

				cv::Affine3d cam_pose = path_est[idx];

				cv::viz::WCameraPosition cpw(0.25); // Coordinate axes
				cv::viz::WCameraPosition cpw_frustum(cv::Matx33d(camera_matrix), 0.3, cv::viz::Color::yellow()); // Camera frustum

				if (camera_pov)
					window_est.setViewerPose(cam_pose);
				else
				{
					// render complete trajectory
					window_est.showWidget("cameras_frames_and_lines_est", cv::viz::WTrajectory(path_est, cv::viz::WTrajectory::PATH, 1.0, cv::viz::Color::green()));

					window_est.showWidget("CPW", cpw, cam_pose);
					window_est.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
				}

				// update trajectory index (spring effect)
				forw *= (idx == n || idx == 0) ? -1 : 1; idx += forw;

				// frame rate 1s
				window_est.spinOnce(1, true);
				window_est.removeAllWidgets();
			}

		}
		::utils::kill(0, "Successfully tried calculating camera extrinsics.");
	}

	// Perform carving

	setup_image_data(
		parser,
		inmode == "camera" ? "memory" : inmode,
		&image_grab,
		&image_data,
		&image_data_video,
		&image_data_images,
		&image_data_memory,
		&image_data_video_input,
		&image_data_memory_input
	);

	float* fArray = new float[voxel_size_x * voxel_size_y * voxel_size_z];
	std::fill_n(fArray, voxel_size_x * voxel_size_y * voxel_size_z, 10.0f);
	fArray = carve(
		parser,
		fArray,
		voxel_size_cell * voxel_size_x,
		voxel_size_cell * voxel_size_y,
		voxel_size_cell * voxel_size_z,
		voxel_size_x,
		voxel_size_y,
		voxel_size_z,
		image_grab,
		image_data,
		bg_color_min, bg_color_max,
		Rs_est, ts_est,
		camera_matrix
	);
	

	{
		// Create vtk visualization pipeline from voxel grid 
		vtkNew<vtkStructuredPoints> sPoints;
		sPoints->SetDimensions(voxel_size_x, voxel_size_y, voxel_size_z);
		sPoints->SetSpacing(1, 1, 1);
		sPoints->SetOrigin(0.0, 0.0, 0.0);

		vtkNew<vtkFloatArray> vtkFArray;
		vtkFArray->SetNumberOfValues(voxel_size_x * voxel_size_y * voxel_size_z);
		vtkFArray->SetArray(fArray, voxel_size_x * voxel_size_y * voxel_size_z, 1);

		sPoints->GetPointData()->SetScalars(vtkFArray);
		sPoints->GetPointData()->Update();

		// Use marching cubes algorithm from lecture
		vtkNew<vtkMarchingCubes> mcSource;
		mcSource->SetInputData(sPoints);
		mcSource->SetNumberOfContours(1);
		mcSource->SetValue(0, 0.5);
		mcSource->Update();

		// Clean mesh topology: remove unused points and merge duplicates
		vtkNew<vtkCleanPolyData> cleanPolyData;
		cleanPolyData->SetInputConnection(mcSource->GetOutputPort());
		cleanPolyData->Update();
		vtkNew<vtkPolyDataMapper> mapper;
		mapper->SetInputConnection(cleanPolyData->GetOutputPort());

		if (outmode == OUT_MODE_VTK)
		{
			vtkNew<vtkActor> actor;

			actor->SetMapper(mapper);

			vtkNew<vtkRenderer> renderer;
			renderer->GradientBackgroundOn();
			renderer->SetBackground(.45, .45, .8);
			renderer->SetBackground2(.0, .0, .0);

			vtkNew<vtkRenderWindow> renderWindow;
			renderWindow->AddRenderer(renderer);
			renderWindow->SetWindowName("Voxel Volume");
			vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
			renderWindowInteractor->SetRenderWindow(renderWindow);

			actor->GetProperty()->SetSpecular(0.2);
			renderer->AddActor(actor);

			{
				vtkNew<vtkNamedColors> colors;

				// Create an image data
				vtkNew<vtkImageData> imageData;

				// Specify the size of the image data
				imageData->SetDimensions(2, 2, 2);
				imageData->SetSpacing(voxel_size_x, voxel_size_y, voxel_size_z);
				imageData->SetOrigin(0.0, 0.0, 0.0);

				vtkNew<vtkDataSetMapper> mapper;
				mapper->SetInputData(imageData);

				vtkNew<vtkActor> actor;
				actor->SetMapper(mapper);
				actor->GetProperty()->SetRepresentationToWireframe();
				actor->GetProperty()->SetColor(colors->GetColor3d("Gold").GetData());

				renderer->AddActor(actor);
			}

			// Render the carved model
			renderWindow->Render();
			renderWindowInteractor->Start();
		}
		else if (outmode == OUT_MODE_FILE)
		{
			auto outfile = parser.get<std::string>("outfile");
			if (outfile.find("ply") != std::string::npos)
			{
				vtkNew<vtkPLYWriter> writer;
				writer->SetFileName(outfile.c_str());
				writer->SetInputConnection(cleanPolyData->GetOutputPort());
				writer->Write();
			}
			else
			{
				vtkNew<vtkPolyDataWriter> writer;
				writer->SetFileName(outfile.c_str());
				writer->SetInputConnection(cleanPolyData->GetOutputPort());
				writer->Write();
			}
		}
	}

	delete[] fArray;
	return 0;
}

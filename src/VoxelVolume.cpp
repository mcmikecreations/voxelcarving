#include "voxelcarving/VoxelVolume.hpp"
#include <Eigen/Dense>
#include <vector>

class VoxelVolumeImpl
{
public:
	std::vector<Eigen::Vector<float, 1>> m_volume;
	// Actual volume size
	float m_sx, m_sy, m_sz, m_sc;
	// Cell count
	int m_cx, m_cy, m_cz;

	VoxelVolumeImpl(float sx, float sy, float sz, float cellSize)
	{
		int x = int(sx / cellSize);
		int y = int(sy / cellSize);
		int z = int(sz / cellSize);
		m_volume.resize(x * y * z);
		m_sc = cellSize;
		m_sx = sx; m_sy = sy; m_sz = sz;
		m_cx = x; m_cy = y; m_cz = z;
	}
};

void VoxelVolume::SetVoxel(float tx, float ty, float tz, float r, float g, float b)
{
	int x = int(tx * m_impl->m_sx * m_impl->m_cx);
	int y = int(ty * m_impl->m_sy * m_impl->m_cy);
	int z = int(tz * m_impl->m_sz * m_impl->m_cz);
	SetVoxel(x, y, z, r, g, b);
}

void VoxelVolume::SetVoxel(int x, int y, int z, float r, float g, float b)
{
	auto& cell = m_impl->m_volume[z * m_impl->m_cx * m_impl->m_cy + y * m_impl->m_cx + x];
	cell[0] = r;
}

void VoxelVolume::ClearVoxel(float tx, float ty, float tz)
{
	int x = int(tx * m_impl->m_sx * m_impl->m_cx);
	int y = int(ty * m_impl->m_sy * m_impl->m_cy);
	int z = int(tz * m_impl->m_sz * m_impl->m_cz);
	m_impl->m_volume[z * m_impl->m_cx * m_impl->m_cy + y * m_impl->m_cx + x][0] = 0;
}

void VoxelVolume::Init()
{
	auto& src = Eigen::Vector<float, 1>();
	src[0] = 1000;
	for (auto& cell : m_impl->m_volume)
	{
 		cell = src;
	}
	
}

void VoxelVolume::Clear()
{
	for (auto& cell : m_impl->m_volume)
	{
		cell[0] = 0;
	}
}

float* VoxelVolume::GetRawVoxelArray()
{
	return (float*)m_impl->m_volume.data();
}

float VoxelVolume::GetSizeX()
{
	return m_impl->m_sx;
}

float VoxelVolume::GetSizeY()
{
	return m_impl->m_sy;
}

float VoxelVolume::GetSizeZ()
{
	return m_impl->m_sz;
}

float VoxelVolume::GetCellSize()
{
	return m_impl->m_sc;
}

int VoxelVolume::GetCellCountX()
{
	return m_impl->m_cx;
}

int VoxelVolume::GetCellCountY()
{
	return m_impl->m_cy;
}

int VoxelVolume::GetCellCountZ()
{
	return m_impl->m_cz;
}

int VoxelVolume::GetCellCountTotal()
{
	return GetCellCountX() * GetCellCountY() * GetCellCountZ();
}

VoxelVolume::VoxelVolume(float sx, float sy, float sz, float cellSize)
{
	m_impl = std::make_unique<VoxelVolumeImpl>(sx, sy, sz, cellSize);
}

VoxelVolume::~VoxelVolume()
{
	m_impl.reset();
}
